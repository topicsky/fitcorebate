package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskyrunnable;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * 所在项目名称 ： topicskycode
 * 操作文件所在包路径　： languagefeature.functionalinterfaces.topicskyrunnable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 03 日 10 时 09 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class BasicProcessImpl implements Serializable{
    /**
     * Init process integer.
     *
     * @param processRunnable
     *         the process predicate
     *
     * @return the integer
     */
    public static Integer initProcess(Runnable processRunnable){
        /*这里 Process 方法 实际 Call 实现过程的地方 这里实现了 Runnable 接口 ，jdk自带的接口*/
        processRunnable.run();
        return 2;
    }
}
