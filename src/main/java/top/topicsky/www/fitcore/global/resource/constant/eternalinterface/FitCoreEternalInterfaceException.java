package top.topicsky.www.fitcore.global.resource.constant.eternalinterface;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * The interface Fit core eternal interface exception.
 */
@Component
@Transactional
public interface FitCoreEternalInterfaceException extends Serializable{
    /**
     * The constant ARITHMETIC.
     */
    String ARITHMETIC="ARITHMETIC(算数异常)";
    /**
     * The constant ARRAYSTORE.
     */
    String ARRAYSTORE="ARRAYSTORE(集合存储异常)";
    /**
     * The constant CLASSNOTFOUND.
     */
    String CLASSNOTFOUND="CLASSNOTFOUND(未匹配类异常)";
    /**
     * The constant EXCEPTION.
     */
    String EXCEPTION="EXCEPTION(全局异常)";
    /**
     * The constant ILLEGALACCESS.
     */
    String ILLEGALACCESS="ILLEGALACCESS(非法接入异常)";
    /**
     * The constant ILLEGALMONITORSTATE.
     */
    String ILLEGALMONITORSTATE="ILLEGALMONITORSTATE(非法监视状态异常)";
    /**
     * The constant ILLEGALTHREADSTATE.
     */
    String ILLEGALTHREADSTATE="ILLEGALTHREADSTATE(非法线程状态异常)";
    /**
     * The constant INSTANTIATION.
     */
    String INSTANTIATION="INSTANTIATION(实例化异常)";
    /**
     * The constant NEGATIVEARRAYSIZE.
     */
    String NEGATIVEARRAYSIZE="NEGATIVEARRAYSIZE(负值数组大小异常)";
    /**
     * The constant NOSUCHMETHOD.
     */
    String NOSUCHMETHOD="NOSUCHMETHOD(没有所引用方法异常)";
    /**
     * The constant NUMBERFORMAT.
     */
    String NUMBERFORMAT="NUMBERFORMAT(数字格式转化格式异常)";
    /**
     * The constant RUNTIM.
     */
    String RUNTIM="RUNTIM(运行时异常)";
    /**
     * The constant STRINGINDEXOUTOFBOUNDS.
     */
    String STRINGINDEXOUTOFBOUNDS="STRINGINDEXOUTOFBOUNDS(字符串索引越界异常)";
    /**
     * The constant UNSUPPORTEDOPERATION.
     */
    String UNSUPPORTEDOPERATION="UNSUPPORTEDOPERATION(不支持的操作异常)";
    /**
     * The constant ARRAYINDEXOUTOFBOUNDS.
     */
    String ARRAYINDEXOUTOFBOUNDS="ARRAYINDEXOUTOFBOUNDS(数组越界异常)";
    /**
     * The constant CLASSCAST.
     */
    String CLASSCAST="CLASSCAST(类型强转异常)";
    /**
     * The constant CLONENOTSUPPORTED.
     */
    String CLONENOTSUPPORTED="CLONENOTSUPPORTED(不支持类异常)";
    /**
     * The constant ENUMCONSTANTNOTPRESENT.
     */
    String ENUMCONSTANTNOTPRESENT="ENUMCONSTANTNOTPRESENT(枚举常量不存在异常)";
    /**
     * The constant GLOBALEXCEPTIONHANDLERINTER.
     */
    String GLOBALEXCEPTIONHANDLERINTER="GLOBALEXCEPTIONHANDLERINTER(这里是全局异常处理)";
    /**
     * The constant ILLEGALARGUMENT.
     */
    String ILLEGALARGUMENT="ILLEGALARGUMENT(非法参数参数)";
    /**
     * The constant ILLEGALSTATE.
     */
    String ILLEGALSTATE="ILLEGALSTATE(非法状态异常)";
    /**
     * The constant INDEXOUTOFBOUNDS.
     */
    String INDEXOUTOFBOUNDS="INDEXOUTOFBOUNDS(索引越界异常)";
    /**
     * The constant INTERRUPTED.
     */
    String INTERRUPTED="INTERRUPTED(异常中断)";
    /**
     * The constant NOSUCHFIELD.
     */
    String NOSUCHFIELD="NOSUCHFIELD(没有这样的域异常)";
    /**
     * The constant NULLPOINTER.
     */
    String NULLPOINTER="NULLPOINTER(空指针异常)";
    /**
     * The constant REFLECTIVEOPERATION.
     */
    String REFLECTIVEOPERATION="REFLECTIVEOPERATION(反射操作异常)";
    /**
     * The constant SECURITY.
     */
    String SECURITY="SECURITY(安全异常)";
    /**
     * The constant TYPENOTPRESENT.
     */
    String TYPENOTPRESENT="TYPENOTPRESENT(类型不存在异常)";
}
