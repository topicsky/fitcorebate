package top.topicsky.www.fitcore.global.community.multithreaded.threadpoolbasicimplement;



import java.io.Serializable;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.threadpoolimplement
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 06 日 10 时 55 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * 测试类
 */
public class FitCoreBasicExecutorMainImpl implements FitCoreBasicExecutorMainInter, Serializable{
    /**
     * Main.
     *
     * @param args
     *         the args
     */
    public static void main(String[] args){
        FitCoreBasicExecutorProcessPool pool=FitCoreBasicExecutorProcessPool.getInstance();
        for(int i=0;i<20;i++){
            Future<?> future=pool.submit(new FitCoreBasicExcuteTask1(i+"号任务"));
            try{
                 /*
                 * 如果接收线程返回值，future.get() 会阻塞
                 * 如果这样写就是一个线程一个线程执行
                 * 所以非特殊情况不建议使用接收返回值的
                 * */
                //SOPL.accept(future.get());
            }catch(Exception e){
                SOPL.accept(e.getMessage());
            }
        }
        for(int i=0;i<20;i++){
            pool.execute(new FitCoreBasicExcuteTask2(i+"号任务"));
        }
        //关闭线程池，如果是需要长期运行的线程池，不用调用该方法。
        //监听程序退出的时候最好执行一下。
        pool.shutdown();
    }

    /**
     * Invock init topicsky.
     */

    public void invockInitTopicsky(){
        /*测试框架*/
        this.initTopicsky();
    }

    @Override
    public void initTopicsky(){
    }

    /**
     * 执行任务1，实现Callable方式
     */
    static class FitCoreBasicExcuteTask1 implements Callable<String>{
        private String taskName;

        /**
         * Instantiates a new Fit core basic excute task 1.
         *
         * @param taskName
         *         the task name
         */
        public FitCoreBasicExcuteTask1(String taskName){
            this.taskName=taskName;
        }

        @Override
        public String call() throws Exception{
            try{
              /*
              * Java 6/7最佳的休眠方法为TimeUnit.MILLISECONDS.sleep(100);
              * 最好不要用 Thread.sleep(100);
              */
                // 1000毫秒以内的随机数，模拟业务逻辑处理
                TimeUnit.MILLISECONDS.sleep((int)(Math.random()*1000));
            }catch(Exception e){
                e.printStackTrace();
            }
            SOPL.accept("执行业务逻辑，Callable 调用 ："+taskName);
            return "线程返回值，Callable 调用 ： = "+taskName;
        }
    }

    /**
     * 执行任务2，实现Runable方式
     */
    static class FitCoreBasicExcuteTask2 implements Runnable{
        private String taskName;

        /**
         * Instantiates a new Fit core basic excute task 2.
         *
         * @param taskName
         *         the task name
         */
        public FitCoreBasicExcuteTask2(String taskName){
            this.taskName=taskName;
        }

        @Override
        public void run(){
            try{
                // 1000毫秒以内的随机数，模拟业务逻辑处理
                TimeUnit.MILLISECONDS.sleep((int)(Math.random()*1000));
            }catch(Exception e){
                SOPL.accept(e.getMessage());
            }
            SOPL.accept("这里执行业务逻辑，Runnable 调用 ： = "+taskName);
        }
    }
}
