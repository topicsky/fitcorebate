package top.topicsky.www.fitcore.global.cache.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.*;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 所在项目名称 ： fitcorebate
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.cache.redis
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 20 日 17 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class RedisUtil{
    @Autowired
    @Qualifier("redisTemplate")
    public RedisTemplate<String,Object> redisTemplate;
    @Autowired
    @Qualifier("redisTemplate")
    protected RedisTemplate<Serializable,Serializable> redisTemplateSerializable;

    /**
     * 缓存基本的对象，Integer、String、实体类等
     */
    public void setCacheObject(String key,Object value){
        redisTemplate.opsForValue().set(key,value);
    }

    /**
     * 获得缓存的基本对象。
     */
    public Object getCacheObject(String key/*,ValueOperations<String,T> operation*/){
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 缓存List数据
     */
    public Object setCacheList(String key,List<Object> dataList){
        ListOperations<String,Object> listOperation=redisTemplate.opsForList();
        if(null!=dataList){
            dataList.stream().forEach((temp)->listOperation.rightPush(key,temp));
        }
        return listOperation;
    }

    /**
     * 获得缓存的list对象
     */
    public List<Object> getCacheList(String key){
        List<Object> dataList=new ArrayList<Object>();
        ListOperations<String,Object> listOperation=redisTemplate.opsForList();
        Long size=listOperation.size(key);
        for(int i=0;i<size;i++){
            dataList.add(listOperation.leftPop(key));
        }
        return dataList;
    }

    /**
     * 获得缓存的list对象
     */
    public List<Object> range(String key,long start,long end){
        ListOperations<String,Object> listOperation=redisTemplate.opsForList();
        return listOperation.range(key,start,end);
    }

    /**
     * list集合长度
     */
    public Long listSize(String key){
        return redisTemplate.opsForList().size(key);
    }

    /**
     * 覆盖操作,将覆盖List中指定位置的值
     */
    public void listSet(String key,int index,Object obj){
        redisTemplate.opsForList().set(key,index,obj);
    }

    /**
     * 向List尾部追加记录
     */
    public long leftPush(String key,Object obj){
        return redisTemplate.opsForList().leftPush(key,obj);
    }

    /**
     * 向List头部追加记录
     */
    public long rightPush(String key,Object obj){
        return redisTemplate.opsForList().rightPush(key,obj);
    }

    /**
     * 删除,只保留start与end之间的记录
     */
    public void trim(String key,int start,int end){
        redisTemplate.opsForList().trim(key,start,end);
    }

    /**
     * 删除List中c条记录，被删除的记录值为value
     */
    public long remove(String key,long i,Object obj){
        return redisTemplate.opsForList().remove(key,i,obj);
    }

    /**
     * 缓存Set
     */
    public BoundSetOperations<String,Object> setCacheSet(String key,Set<Object> dataSet){
        BoundSetOperations<String,Object> setOperation=redisTemplate.boundSetOps(key);
        dataSet.stream().forEach((temp)->setOperation.add(temp));
        return setOperation;
    }

    /**
     * 获得缓存的set
     */
    public Set<Object> getCacheSet(String key){
        Set<Object> dataSet=new HashSet<Object>();
        BoundSetOperations<String,Object> operation=redisTemplate.boundSetOps(key);
        Long size=operation.size();
        for(int i=0;i<size;i++){
            dataSet.add(operation.pop());
        }
        return dataSet;
    }

    /**
     * 缓存Map
     */
    public int setCacheMap(String key,Map<String,Object> dataMap){
        if(null!=dataMap){
            HashOperations<String,Object,Object> hashOperations=redisTemplate.opsForHash();
            for(Map.Entry<String,Object> entry : dataMap.entrySet()){
                if(hashOperations!=null){
                    hashOperations.put(key,entry.getKey(),entry.getValue());
                }else{
                    return 0;
                }
            }
        }else{
            return 0;
        }
        return dataMap.size();
    }

    /**
     * 获得缓存的Map
     */
    public Map<Object,Object> getCacheMap(String key){
        Map<Object,Object> map=redisTemplate.opsForHash().entries(key);
        return map;
    }

    /**
     * 缓存Map
     */
    public void setCacheIntegerMap(String key,Map<Integer,Object> dataMap){
        HashOperations<String,Object,Object> hashOperations=redisTemplate.opsForHash();
        if(null!=dataMap){
            for(Map.Entry<Integer,Object> entry : dataMap.entrySet()){
                hashOperations.put(key,entry.getKey(),entry.getValue());
            }
        }
    }

    /**
     * 获得缓存的Map
     */
    public Map<Object,Object> getCacheIntegerMap(String key){
        Map<Object,Object> map=redisTemplate.opsForHash().entries(key);
        return map;
    }

    /**
     * 从hash中删除指定的存储
     */
    public long deleteMap(String key){
        redisTemplate.setEnableTransactionSupport(true);
        return redisTemplate.opsForHash().delete(key);
    }

    /**
     * 设置过期时间
     */
    public boolean expire(String key,long time,TimeUnit unit){
        return redisTemplate.expire(key,time,unit);
    }

    /**
     * increment
     */
    public long increment(String key,long step){
        return redisTemplate.opsForValue().increment(key,step);
    }

    /**
     * 删除redis的所有数据
     */
    /*@SuppressWarnings({"unchecked", "rawtypes"})
    public String flushDB() {
        return redisTemplateSerializable.execute(new RedisCallback() {
            public String doInRedis(RedisConnection connection) throws DataAccessException {
                connection.flushDb();
                return "ok";
            }
        });
    }*/
    public long del(final byte[] key){
        return (long)redisTemplateSerializable.execute(new RedisCallback<Object>(){
            public Long doInRedis(RedisConnection connection){
                return connection.del(key);
            }
        });
    }

    @SuppressWarnings({"unchecked","rawtypes"})
    public byte[] get(final byte[] key){
        return (byte[])redisTemplateSerializable.execute(new RedisCallback(){
            public byte[] doInRedis(RedisConnection connection) throws DataAccessException{
                return connection.get(key);
            }
        });
    }

    /**
     */
    public void set(final byte[] key,final byte[] value,final long liveTime){
        redisTemplateSerializable.execute(new RedisCallback<Object>(){
            public Long doInRedis(RedisConnection connection) throws DataAccessException{
                connection.set(key,value);
                if(liveTime>0){
                    connection.expire(key,liveTime);
                }
                return 1L;
            }
        });
    }
}
