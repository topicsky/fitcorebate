package top.topicsky.www.fitcore.global.util.predicate.string;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;
import static top.topicsky.www.fitcore.system.kernel.common.supplier.SupplierUtil.GAL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.predicate.string
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 14 时 58 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicStringPredicateImpl implements Serializable{
    /**
     * Split predicate string [ ].
     *
     * @param strings
     *         the strings
     *
     * @return the string [ ]
     */
    public static String[] splitPredicate(String[] strings){
        int index;
        ArrayList al=GAL.get();
        while((index=strings[0].indexOf(strings[1]))!=-1){
            al.add(strings[0].substring(0,index));
            strings[0]=strings[0].substring(index+strings[1].length());
        }
        al.add(strings[0]);
        return (String[])al.toArray(new String[0]);
    }

    /**
     * Replace predicate string.
     *
     * @param strings
     *         the strings
     *
     * @return the string
     */
    public static String replacePredicate(String[] strings){
        StringBuffer str=new StringBuffer("");
        int index=-1;
        while((index=strings[2].indexOf(strings[0]))!=-1){
            str.append(strings[2].substring(0,index)+strings[1]);
            strings[2]=strings[2].substring(index+strings[0].length());
            index=strings[2].indexOf(strings[0]);
        }
        return OTS.apply(str.append(strings[2]));
    }
}
