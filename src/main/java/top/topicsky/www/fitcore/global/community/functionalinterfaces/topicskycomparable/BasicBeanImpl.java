package top.topicsky.www.fitcore.global.community.functionalinterfaces.topicskycomparable;


import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： topicskycode
 * 操作文件所在包路径　： languagefeature.functionalinterfaces.topicskycomparable
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 03 日 17 时 52 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class BasicBeanImpl{
    /**
     * Invock comparable.
     */

    public void invockComparable(){
        /*测试框架*/
        Supplier<BasicBean> basicBeana=()->new BasicBean("tool",03,88.88D);
        Supplier<BasicBean> basicBeanb=()->new BasicBean("plugs",04,65.88D);
        Supplier<BasicBean> basicBeanc=()->new BasicBean("core",02,89.880D);
        Supplier<BasicBean> basicBeand=()->new BasicBean("kernel",01,100.0D);
        BasicBean basicBeans[]={
                basicBeana.get(),
                basicBeanb.get(),
                basicBeanc.get(),
                basicBeand.get()};
        Arrays.sort(basicBeans);
        for(BasicBean basicBean : basicBeans){
            SOPL.accept(basicBean);
        }
    }
}
