package top.topicsky.www.fitcore.global.interceptor.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import top.topicsky.www.fitcore.global.interceptor.inter.FitCoreHandlerWebRequestInterceptorCustomInter;
import top.topicsky.www.fitcore.global.interceptor.predicate.FitCoreHandlerWebRequestInterceptorCustomAbstractPredicateImpl;
import top.topicsky.www.fitcore.global.interceptor.predicate.FitCoreHandlerWebRequestInterceptorCustomPredicateImpl;
import top.topicsky.www.fitcore.global.interceptor.process.FitCoreHandlerWebRequestInterceptorCustomAbstractProcessImpl;
import top.topicsky.www.fitcore.global.interceptor.process.FitCoreHandlerWebRequestInterceptorCustomProcessImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.interceptor.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 23 日 14 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public abstract class FitCoreHandlerWebRequestInterceptorCustomImpl
        implements FitCoreHandlerWebRequestInterceptorCustomInter, Serializable{
    private static final long serialVersionUID=-9079424051115827235L;

    /**
     * SCOPE_REQUEST ：它的值是0
     * 代表只有在request 中可以访问
     * <p>
     * SCOPE_SESSION ：它的值是1
     * 如果环境允许的话它代表的是一个局部的隔离的session
     * 否则就代表普通的session，并且在该session范围内可以访问
     * <p>
     * SCOPE_GLOBAL_SESSION ：它的值是2
     * 如果环境允许的话，它代表的是一个全局共享的session
     * 否则就代表普通的session，并且在该session 范围内可以访问
     */
    public FitCoreHandlerWebRequestInterceptorCustomImpl(){
    }

    /**
     * Fit core handler web request interceptor custom init.
     */
    public void FitCoreHandlerWebRequestInterceptorCustomInit(){
        FitCoreHandlerWebRequestInterceptorCustomAbstractProcessImpl.FitCoreHandlerWebRequestInterceptorAbstractCustomInit("initAbstract_WebRequest_Custom",
                FitCoreHandlerWebRequestInterceptorCustomAbstractPredicateImpl::FitCoreHandlerWebRequestInterceptorAbstractCustomInit);
    }

    @Override
    public void preHandle(WebRequest webRequest) throws Exception{
        Runnable runnable=()->FitCoreHandlerWebRequestInterceptorCustomProcessImpl.FitCoreHandlerWebRequestInterceptorCustomInit("preHandle_WebRequest_Custom",
                FitCoreHandlerWebRequestInterceptorCustomPredicateImpl::FitCoreHandlerWebRequestInterceptorCustomInit);
        runnable.run();
    }

    @Override
    public void postHandle(WebRequest webRequest,ModelMap modelMap) throws Exception{
        Runnable runnable=()->FitCoreHandlerWebRequestInterceptorCustomProcessImpl.FitCoreHandlerWebRequestInterceptorCustomInit("postHandle_WebRequest_Custom",
                FitCoreHandlerWebRequestInterceptorCustomPredicateImpl::FitCoreHandlerWebRequestInterceptorCustomInit);
        runnable.run();
    }

    @Override
    public void afterCompletion(WebRequest webRequest,Exception e) throws Exception{
        Runnable runnable=()->FitCoreHandlerWebRequestInterceptorCustomProcessImpl.FitCoreHandlerWebRequestInterceptorCustomInit("afterCompletion_WebRequest_Custom",
                FitCoreHandlerWebRequestInterceptorCustomPredicateImpl::FitCoreHandlerWebRequestInterceptorCustomInit);
        runnable.run();
    }
}
