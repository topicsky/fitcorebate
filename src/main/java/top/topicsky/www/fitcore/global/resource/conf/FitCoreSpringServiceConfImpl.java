package top.topicsky.www.fitcore.global.resource.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.procedure.service.impl.FitCoreUserServiceImpl;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.resource.conf
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 16 日 19 时 48 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
@Configuration
public class FitCoreSpringServiceConfImpl implements Serializable{
    /**
     * Fit core user service impla fit core user service.
     *
     * @return the fit core user service
     */
    @Bean(name="fitCoreUserServiceImpl_Constructor_NoParama")
    public FitCoreUserServiceImpl fitCoreUserServiceImpla(){
        return new FitCoreUserServiceImpl();
    }

    /**
     * Fit core user service implb fit core user service.
     *
     * @return the fit core user service
     */
    @Bean(name="fitCoreUserServiceImpl_Constructor_NoParamb")
    public FitCoreUserServiceImpl fitCoreUserServiceImplb(){
        return new FitCoreUserServiceImpl();
    }

    /**
     * Fit core user service implc fit core user service.
     *
     * @return the fit core user service
     */
    @Bean(name="fitCoreUserServiceImpl_Constructor_NoParamc")
    public FitCoreUserServiceImpl fitCoreUserServiceImplc(){
        return new FitCoreUserServiceImpl();
    }
}
