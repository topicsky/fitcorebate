package top.topicsky.www.fitcore.global.util.impl.plugs.generateexcel;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.document.AbstractExcelView;
import top.topicsky.www.fitcore.global.util.inter.plugs.generateexcel.FitCoreGenerateExcelSourceInter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Map;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.plugs.generateexcel
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 17 日 13 时 06 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitCoreGenerateExcelProcessImpl extends AbstractExcelView implements FitCoreGenerateExcelSourceInter, Serializable{
    @Override
    public void buildExcelDocument(Map<String,Object> model,
                                   HSSFWorkbook workbook,HttpServletRequest request,HttpServletResponse response)
            throws Exception{
        Map<String,String> userData=(Map<String,String>)model.get("fitCoreLogInFormDtoDate");
        //create a wordsheet
        HSSFSheet sheet=workbook.createSheet("User Report");
        HSSFRow header=sheet.createRow(0);
        header.createCell(0).setCellValue("编号");
        header.createCell(1).setCellValue("姓名");
        int rowNum=1;
        for(Map.Entry<String,String> entry : userData.entrySet()){
            //create the row data
            HSSFRow row=sheet.createRow(rowNum++);
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
            row.createCell(0).setCellValue(entry.getKey());
            row.createCell(1).setCellValue(entry.getValue());
        }
    }
}
