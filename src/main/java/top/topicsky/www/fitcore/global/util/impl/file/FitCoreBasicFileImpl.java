package top.topicsky.www.fitcore.global.util.impl.file;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.util.inter.file.FitCoreBasicFileInter;
import top.topicsky.www.fitcore.global.util.predicate.file.FitCoreBasicFilePredicateImpl;
import top.topicsky.www.fitcore.global.util.process.file.FitCoreBasicFileProcessImpl;

import java.io.File;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.util.impl.file
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 04 日 15 时 33 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicFileImpl implements FitCoreBasicFileInter, Serializable{
    /**
     * 功能描述：列出某文件夹及其子文件夹下面的文件，并可根据扩展名过滤
     *
     * @param path
     *         文件夹
     */
    public static void list(File path){
        FitCoreBasicFileProcessImpl.listProcess(path,
                FitCoreBasicFilePredicateImpl::listPredicate);
    }

    /**
     * 功能描述：拷贝一个目录或者文件到指定路径下，即把源文件拷贝到目标文件路径下
     *
     * @param source
     *         源文件
     * @param target
     *         目标文件路径
     *
     * @return void
     */
    public static void copy(File source,File target){
        FitCoreBasicFileProcessImpl.copyProcess(source,
                target,
                FitCoreBasicFilePredicateImpl::copyPredicate);
    }
}
