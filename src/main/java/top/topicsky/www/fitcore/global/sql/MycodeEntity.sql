-- auto Generated on 2017-06-15 23:05:47 
-- DROP TABLE IF EXISTS `mycode_entity`; 
CREATE TABLE `mycode_entity`(
    `my_code_id` BIGINT (15) NOT NULL AUTO_INCREMENT COMMENT 'myCodeId',
    `my_code_name` VARCHAR (512) NOT NULL DEFAULT '' COMMENT 'myCodeName',
    `my_code_age` VARCHAR (32) NOT NULL DEFAULT '' COMMENT 'myCodeAge',
    `my_code_iphone` VARCHAR (32) NOT NULL DEFAULT '' COMMENT 'myCodeIphone',
    `mu_code_address` VARCHAR (1024) NOT NULL DEFAULT '' COMMENT 'muCodeAddress',
    PRIMARY KEY (`my_code_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`mycode_entity`';
