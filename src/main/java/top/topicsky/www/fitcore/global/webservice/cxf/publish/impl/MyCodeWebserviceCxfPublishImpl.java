package top.topicsky.www.fitcore.global.webservice.cxf.publish.impl;

import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import top.topicsky.www.fitcore.global.webservice.cxf.impl.MyCodeWebServiceCxfImpl;
import top.topicsky.www.fitcore.global.webservice.cxf.inter.MyCodeWebServiceCxfInter;
import top.topicsky.www.fitcore.global.webservice.cxf.publish.inter.MyCodeWebserviceCxfPublishInter;

import java.io.Serializable;
/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.webservice.cxf.publish.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 03 日 13 时 18 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */

/**
 * CXF WebService
 * <p>
 * 注意:使用CXF发布webService 接口和实现类都需要webService注解
 */
@Component
@Transactional
@RequestMapping("/myCodeWebserviceCxfPublishImpl")
public class MyCodeWebserviceCxfPublishImpl implements MyCodeWebserviceCxfPublishInter, Serializable{
    /*#####################################注入控制层######################################################*/

    /*#####################################注入控制层######################################################*/
    /*#####################################注入业务层######################################################*/


    /*#####################################注入业务层######################################################*/
    /*#####################################注入持久层######################################################*/

    /*#####################################注入持久层######################################################*/
    /*#####################################注入数据层######################################################*/

    /*#####################################注入数据层######################################################*/
    /*#####################################依赖配置类######################################################*/

    /*#####################################依赖配置类######################################################*/
    /*#####################################自定义注解######################################################*/

    /*#####################################自定义注解######################################################*/

    @Override
    @RequestMapping(value="/publish", method=RequestMethod.GET)
    public void publish(){
        /**
         * 发布服务
         * 参数一 : 服务地址是谁
         * 参数二 : 服务的实现类
         * URL : http://127.0.0.1:9999/ws?wsdl
         */
        //创建服务发布类对象
        // 服务发布实例自定义 Bean Publish
        JaxWsServerFactoryBean jaxWsServerFactoryBeanCxf=new JaxWsServerFactoryBean();
        //服务的发布地址
        jaxWsServerFactoryBeanCxf.setAddress("http://127.0.0.1:19999/ws");
        //服务的接口类型
        jaxWsServerFactoryBeanCxf.setServiceClass(MyCodeWebServiceCxfInter.class);
        //服务的实现类
        jaxWsServerFactoryBeanCxf.setServiceBean(new MyCodeWebServiceCxfImpl());
        //发布服务
        jaxWsServerFactoryBeanCxf.create();
    }
}
