package top.topicsky.www.fitcore.global.community.multithreaded.threadpoolintermediateimplement;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.community.multithreaded.threadpoolimplement
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 05 日 15 时 41 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FitCoreInterMediateThreadPoolImpl implements Serializable{
    /**
     * The constant worker_num.
     * 默认线程池中线程数上限
     */
    public static int worker_num=5;
    /**
     * The constant taskList.
     * 任务队列，公共资源
     */
    public static List<FitCoreInterMediateThreadTaskImpl> taskList=Collections.synchronizedList(new LinkedList<FitCoreInterMediateThreadTaskImpl>());
    /**
     * 单例
     */
    private static FitCoreInterMediateThreadPoolImpl instance=FitCoreInterMediateThreadPoolImpl.getInstance();
    /**
     * 已经处理的任务数计数，为 task 设置 Id
     */
    private static int taskCounter=0;
    /**
     * The Workers.
     * 池中的所有激活线程
     */
    public PoolWorker[] workers;

    /**
     * 空构造，默认激活数
     */
    private FitCoreInterMediateThreadPoolImpl(){
        workers=new PoolWorker[5];
        for(int i=0;i<workers.length;i++){
            workers[i]=new PoolWorker(i);
        }
    }

    /**
     * 构造需要激活进程数
     */
    private FitCoreInterMediateThreadPoolImpl(int pool_worker_num){
        worker_num=pool_worker_num;
        workers=new PoolWorker[worker_num];
        for(int i=0;i<workers.length;i++){
            workers[i]=new PoolWorker(i);
        }
    }

    /**
     * Get instance fit core inter mediate thread pool.
     *
     * @return the fit core inter mediate thread pool 取得线程池实例，静态同步资源，单实例顺序执行
     */
    public static synchronized FitCoreInterMediateThreadPoolImpl getInstance(){
        if(instance==null){
            return new FitCoreInterMediateThreadPoolImpl();
        }
        return instance;
    }

    /**
     * 增加新的任务
     * 每增加一个新任务，都要唤醒任务队列
     *
     * @param newTask
     *         the new task
     */
    public void addTask(FitCoreInterMediateThreadTaskImpl newTask){
        synchronized(taskList){
            newTask.setTaskId(++taskCounter);
            newTask.setSubmitTime(new Date());
            taskList.add(newTask);
            /* 唤醒队列*/
            taskList.notifyAll();
        }
        SOPL.accept("提交 新任务，编号：<"+newTask.getTaskId()+">: "+newTask.info());
    }

    /**
     * 批量增加新任务
     *
     * @param taskes
     *         the taskes
     */
    public void batchAddTask(FitCoreInterMediateThreadTaskImpl[] taskes){
        if(taskes==null||taskes.length==0){
            return;
        }
        synchronized(taskList){
            for(int i=0;i<taskes.length;i++){
                if(taskes[i]==null){
                    continue;
                }
                taskes[i].setTaskId(++taskCounter);
                taskes[i].setSubmitTime(new Date());
                taskList.add(taskes[i]);
            }
            /*唤醒队列*/
            taskList.notifyAll();
        }
        for(int i=0;i<taskes.length;i++){
            if(taskes[i]==null){
                continue;
            }
            SOPL.accept("\n提交新任务，编号：<"+taskes[i].getTaskId()+">: "+taskes[i].info());
        }
    }

    /**
     * 线程池信息
     *
     * @return string string
     */
    public String getInfo(){
        StringBuffer sb=new StringBuffer();
        sb.append("\n任务 队列 大小:"+taskList.size());
        for(int i=0;i<workers.length;i++){
            sb.append("\n激活空间编号 "+i+" 状态 "+((workers[i].isWaiting())?"可利用":"被占用"));
        }
        return sb.toString();
    }

    /**
     * 销毁线程池
     */
    public synchronized void destroy(){
        for(int i=0;i<worker_num;i++){
            workers[i].stopWorker();
            SOPL.accept("清除激活工作空间："+workers[i]);
            workers[i]=null;
        }
        taskList.clear();
        SOPL.accept("清空任务队列完毕");
    }

    /**
     * 池中工作线程
     *
     * @author obullxl
     */
    public class PoolWorker extends Thread{
        private int index=-1;
        /* 该工作线程是否正在执行工作 */
        private boolean isRunning=true;
        /* 该工作线程是否可以执行新任务，在等待新任务加入*/
        private boolean isWaiting=false;

        /**
         * Instantiates a new Pool worker.
         *
         * @param index
         *         the index         构造工作进程
         */
        public PoolWorker(int index){
            this.index=index;
            start();
        }

        @Override
        public synchronized void start(){
            super.start();
            SOPL.accept("创建线程并执行，线程调用 run()");
        }

        /**
         * 循环执行任务
         * 这也许是线程池的关键所在
         */
        public void run(){
            while(isRunning){
                FitCoreInterMediateThreadTaskImpl fitCoreInterMediateThreadTask=null;
                synchronized(taskList){
                    while(taskList.isEmpty()){
                        try{
                            /* 任务队列为空，则等待有新任务加入从而被唤醒 */
                            taskList.wait(20);
                        }catch(InterruptedException ie){
                            SOPL.accept(ie);
                        }
                    }
                    /* 取出任务执行 */
                    fitCoreInterMediateThreadTask=taskList.remove(0);
                    SOPL.accept("取出任务:"+fitCoreInterMediateThreadTask.getTaskId()+"，准备执行");
                }
                if(fitCoreInterMediateThreadTask!=null){
                    isWaiting=false;
                    try{
                        /* 该任务是否需要立即执行 */
                        if(fitCoreInterMediateThreadTask.needExecuteImmediate()){
                            SOPL.accept("立即创建线程");
                            new Thread(fitCoreInterMediateThreadTask).start();
                            /*模拟单个线程需要的执行时间*/
                            Thread.sleep(3000);
                        }else{
                            SOPL.accept("不立即执行线程");
                            new Thread(fitCoreInterMediateThreadTask);
                            Thread.sleep(3000);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        SOPL.accept(e);
                    }
                    isWaiting=true;
                    fitCoreInterMediateThreadTask=null;
                }
            }
        }

        @Override
        public String toString(){
            return super.toString();
        }

        /**
         * Stop worker.
         * 停止工作进程
         */
        public void stopWorker(){
            this.isRunning=false;
        }

        /**
         * Is waiting boolean.
         *
         * @return the boolean 取得等待状态
         */
        public boolean isWaiting(){
            return this.isWaiting;
        }
    }
}
