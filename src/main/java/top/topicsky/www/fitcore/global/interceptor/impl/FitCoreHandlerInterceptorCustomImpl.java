package top.topicsky.www.fitcore.global.interceptor.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import top.topicsky.www.fitcore.global.interceptor.inter.FitCoreHandlerInterceptorCustomInter;
import top.topicsky.www.fitcore.global.interceptor.predicate.FitCoreHandlerInterceptorCustomAbstractPredicateImpl;
import top.topicsky.www.fitcore.global.interceptor.predicate.FitCoreHandlerInterceptorCustomPredicateImpl;
import top.topicsky.www.fitcore.global.interceptor.process.FitCoreHandlerInterceptorCustomAbstractProcessImpl;
import top.topicsky.www.fitcore.global.interceptor.process.FitCoreHandlerInterceptorCustomProcessImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.global.interceptor.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 23 日 14 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreHandlerInterceptorCustomImpl implements FitCoreHandlerInterceptorCustomInter, Serializable{
    private static final long serialVersionUID=-1765629482625356982L;

    /**
     * Instantiates a new Fit core handler interceptor custom.
     */
    public FitCoreHandlerInterceptorCustomImpl(){
    }

    /**
     * Fit core handler interceptor custom init.
     */
    public void FitCoreHandlerInterceptorCustomInit(){
        FitCoreHandlerInterceptorCustomAbstractProcessImpl.FitCoreHandlerInterceptorAbstractCustomInit("initAbstract_Custom",
                FitCoreHandlerInterceptorCustomAbstractPredicateImpl::FitCoreHandlerInterceptorAbstractCustomInit);
    }

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o) throws Exception{
        Runnable runnable=()->FitCoreHandlerInterceptorCustomProcessImpl.FitCoreHandlerInterceptorCustomInit("preHandle_Custom",
                FitCoreHandlerInterceptorCustomPredicateImpl::FitCoreHandlerInterceptorCustomInit);
        runnable.run();
        /*这里定义是否有效执行，布尔值*/
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o,ModelAndView modelAndView) throws Exception{
        Runnable runnable=()->FitCoreHandlerInterceptorCustomProcessImpl.FitCoreHandlerInterceptorCustomInit("postHandle_Custom",
                FitCoreHandlerInterceptorCustomPredicateImpl::FitCoreHandlerInterceptorCustomInit);
        runnable.run();
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,HttpServletResponse httpServletResponse,Object o,Exception e) throws Exception{
        Runnable runnable=()->FitCoreHandlerInterceptorCustomProcessImpl.FitCoreHandlerInterceptorCustomInit("afterCompletion_Custom",
                FitCoreHandlerInterceptorCustomPredicateImpl::FitCoreHandlerInterceptorCustomInit);
        runnable.run();
    }
}
