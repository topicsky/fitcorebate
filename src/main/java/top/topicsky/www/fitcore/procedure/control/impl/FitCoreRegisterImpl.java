package top.topicsky.www.fitcore.procedure.control.impl;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import top.topicsky.www.fitcore.procedure.control.inter.FitCoreRegisterInter;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.impl
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 19 日 11 时 17 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
@RequestMapping("/Register")
public class FitCoreRegisterImpl implements FitCoreRegisterInter, Serializable{
    /**
     * Register init string.
     *
     * @param initString
     *         the init string
     * @param model
     *         the model
     *
     * @return the string
     */
    @RequestMapping(value="/{Init}")
    public String registerInit(@PathVariable String initString,Model model){
        return "";
    }
}
