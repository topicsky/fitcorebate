package top.topicsky.www.fitcore.procedure.control.impl;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.procedure.control.inter.FitLogInIndexFilterInter;
import top.topicsky.www.fitcore.procedure.control.predicate.FitLogInIndexFilterPredicateImpl;
import top.topicsky.www.fitcore.procedure.control.process.FitLogInIndexFilterProcessImpl;

import javax.servlet.*;
import java.io.IOException;
import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.procedure.control.impl.system.login
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 05 日 11 时 24 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Controller
@Transactional
public class FitLogInIndexFilterImpl implements FitLogInIndexFilterInter, Filter, Serializable{
    public void init(FilterConfig arg0) throws ServletException{
    }

    public void doFilter(ServletRequest arg0,ServletResponse arg1,
                         FilterChain arg2) throws IOException, ServletException{
        FitLogInIndexFilterProcessImpl.fitLogInIndexFilterProcessGetFilter(
                FitLogInIndexFilterProcessImpl.fitLogInIndexFilterProcessGetFilterInit(
                        arg0,
                        arg1,
                        arg2,
                        FitLogInIndexFilterPredicateImpl::getFilter));
    }

    public void destroy(){
    }
}
