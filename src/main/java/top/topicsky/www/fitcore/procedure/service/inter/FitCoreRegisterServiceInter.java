package top.topicsky.www.fitcore.procedure.service.inter;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.global.entity.FitCoreUserEntity;

import java.util.List;

/**
 * The interface Fit core user service inter.
 */
@Service
@Transactional
public interface FitCoreRegisterServiceInter{
    /**
     * Select log in int.
     *
     * @param pojo
     *         the fit core cname
     *
     * @return the int
     */
    FitCoreUserEntity selectLogIn(FitCoreUserEntity pojo);

    /**
     * Insert int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insert(FitCoreUserEntity pojo);

    /**
     * Insert selective int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int insertSelective(FitCoreUserEntity pojo);

    /**
     * Insert list int.
     *
     * @param pojos
     *         the pojos
     *
     * @return the int
     */
    int insertList(List<FitCoreUserEntity> pojos);

    /**
     * Update int.
     *
     * @param pojo
     *         the pojo
     *
     * @return the int
     */
    int update(FitCoreUserEntity pojo);
}
