package top.topicsky.www.fitcore.system.kernel.modellibrary.creation.factorymethod.staticf;

import top.topicsky.www.fitcore.system.kernel.modellibrary.creation.factorymethod.multi.FitCoreBasicMultiInter;
import top.topicsky.www.fitcore.system.kernel.modellibrary.creation.factorymethod.multi.FitCoreInheritMultiImpl;

import java.util.function.Supplier;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.creation.factorymethod.constant
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 27 日 15 时 46 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FitCoreBasicStaticImpl{
    /**
     * Init multi core xa fit core basic multi inter.
     *
     * @return the fit core basic multi inter
     */
    public static FitCoreBasicMultiInter initMulti_Core_XA(){
        Supplier<FitCoreInheritMultiImpl> supplier=()->new FitCoreInheritMultiImpl();
        return supplier.get();
    }

    /**
     * Init multi core xb fit core basic multi inter.
     *
     * @return the fit core basic multi inter
     */
    public static FitCoreBasicMultiInter initMulti_Core_XB(){
        Supplier<FitCoreInheritMultiImpl> supplier=()->new FitCoreInheritMultiImpl();
        return supplier.get();
    }
}
