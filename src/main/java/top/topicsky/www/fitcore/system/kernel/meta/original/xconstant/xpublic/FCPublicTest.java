package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic;

import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.invock.FCVariableInvockPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.invock.FCWeekInvockPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCWeekModelPublic;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 43 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FCPublicTest{
    /**
     * The entry point of application.
     *
     * @param args
     *         the input arguments
     *
     * @throws Exception
     *         the exception
     */
    public static void main(String[] args) throws Exception{
        //SOPL.accept(FCAssertInvockPublic.getInitClass(FCAssertModelPublic.class));
        //FCAssertModelPublic fcAssertModelPublic=new FCAssertModelPublic();
        //fcAssertModelPublic.setAssertSizeZero(ASSERTSIZEZERO);
        //SOPL.accept(FCAssertInvockPublic.getInitObject(fcAssertModelPublic));
        //SOPL.accept(FCExceptionInvockPublic.getInitClass(FCExceptionModelPublic.class));
        //FCExceptionModelPublic fcExceptionModelPublic=new FCExceptionModelPublic();
        //fcExceptionModelPublic.setExceptionNoMetaDefault(EXCEPTION);
        //fcExceptionModelPublic.setExceptionNoMetaARITHMETIC(ARITHMETIC);
        //fcExceptionModelPublic.setExceptionNoMetaARRAYSTORE(ARRAYSTORE);
        //fcExceptionModelPublic.setExceptionNoMetaCLASSNOTFOUND(CLASSNOTFOUND);
        //fcExceptionModelPublic.setExceptionNoMetaILLEGALACCESS(ILLEGALACCESS);
        //fcExceptionModelPublic.setExceptionNoMetaILLEGALMONITORSTATE(ILLEGALMONITORSTATE);
        //fcExceptionModelPublic.setExceptionNoMetaILLEGALTHREADSTATE(ILLEGALTHREADSTATE);
        //fcExceptionModelPublic.setExceptionNoMetaINSTANTIATION(INSTANTIATION);
        //fcExceptionModelPublic.setExceptionNoMetaNEGATIVEARRAYSIZE(NEGATIVEARRAYSIZE);
        //fcExceptionModelPublic.setExceptionNoMetaNOSUCHMETHOD(NOSUCHMETHOD);
        //fcExceptionModelPublic.setExceptionNoMetaNUMBERFORMAT(NUMBERFORMAT);
        //fcExceptionModelPublic.setExceptionNoMetaRUNTIM(RUNTIM);
        //fcExceptionModelPublic.setExceptionNoMetaSTRINGINDEXOUTOFBOUNDS(STRINGINDEXOUTOFBOUNDS);
        //fcExceptionModelPublic.setExceptionNoMetaUNSUPPORTEDOPERATION(UNSUPPORTEDOPERATION);
        //fcExceptionModelPublic.setExceptionNoMetaARRAYINDEXOUTOFBOUNDS(ARRAYINDEXOUTOFBOUNDS);
        //fcExceptionModelPublic.setExceptionNoMetaCLASSCAST(CLASSCAST);
        //fcExceptionModelPublic.setExceptionNoMetaCLONENOTSUPPORTED(CLONENOTSUPPORTED);
        //fcExceptionModelPublic.setExceptionNoMetaENUMCONSTANTNOTPRESENT(ENUMCONSTANTNOTPRESENT);
        //fcExceptionModelPublic.setExceptionNoMetaGLOBALEXCEPTIONHANDLERINTER(GLOBALEXCEPTIONHANDLERINTER);
        //fcExceptionModelPublic.setExceptionNoMetaILLEGALARGUMENT(ILLEGALARGUMENT);
        //fcExceptionModelPublic.setExceptionNoMetaILLEGALSTATE(ILLEGALSTATE);
        //fcExceptionModelPublic.setExceptionNoMetaINDEXOUTOFBOUNDS(INDEXOUTOFBOUNDS);
        //fcExceptionModelPublic.setExceptionNoMetaINTERRUPTED(INTERRUPTED);
        //fcExceptionModelPublic.setExceptionNoMetaNOSUCHFIELD(NOSUCHFIELD);
        //fcExceptionModelPublic.setExceptionNoMetaNULLPOINTER(NULLPOINTER);
        //fcExceptionModelPublic.setExceptionNoMetaREFLECTIVEOPERATION(REFLECTIVEOPERATION);
        //fcExceptionModelPublic.setExceptionNoMetaSECURITY(SECURITY);
        //fcExceptionModelPublic.setExceptionNoMetaTYPENOTPRESENT(TYPENOTPRESENT);
        //SOPL.accept(FCExceptionInvockPublic.getInitObject(fcExceptionModelPublic));
        //SOPL.accept(FCExtensionNameInvockPublic.getInitClass(FCExtensionNameModelPublic.class));
        //FCExtensionNameModelPublic fcExtensionNameModelPublic=new FCExtensionNameModelPublic();
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITPDF(FITPDF);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITTXT(FITTXT);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITDOC(FITDOC);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITBMP(FITBMP);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITHTML(FITHTML);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITHTM(FITHTM);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITCHM(FITCHM);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITJPG(FITJPG);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITJPGE(FITJPGE);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITPNG(FITPNG);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITJS(FITJS);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITJAVA(FITJAVA);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITC(FITC);
        //fcExtensionNameModelPublic.setExtensionNameNoMetaFITRB(FITRB);
        //
        //SOPL.accept(FCExtensionNameInvockPublic.getInitObject(fcExtensionNameModelPublic));
        //SOPL.accept(FCLayoutInvockPublic.getInitClass(FCLayoutModelPublic.class));
        //FCLayoutModelPublic fcLayoutModelPublic=new FCLayoutModelPublic();
        //fcLayoutModelPublic.setLayoutNoMetaINDEXJSP(INDEXJSP);
        //fcLayoutModelPublic.setLayoutNoMetaERRORJSP(ERRORJSP);
        //SOPL.accept(FCLayoutInvockPublic.getInitObject(fcLayoutModelPublic));
        //SOPL.accept(FCNumberInvockPublic.getInitClass(FCNumberModelPublic.class));
        //FCNumberModelPublic fcNumberModelPublic=new FCNumberModelPublic();
        //fcNumberModelPublic.setNumberNoMetaFITZERO(FITZERO);
        //fcNumberModelPublic.setNumberNoMetaFITONE(FITONE);
        //fcNumberModelPublic.setNumberNoMetaFITTWO(FITTWO);
        //fcNumberModelPublic.setNumberNoMetaFITTHREE(FITTHREE);
        //fcNumberModelPublic.setNumberNoMetaFITFOUR(FITFOUR);
        //fcNumberModelPublic.setNumberNoMetaFITFIVE(FITFIVE);
        //fcNumberModelPublic.setNumberNoMetaFITSIX(FITSIX);
        //fcNumberModelPublic.setNumberNoMetaFITSEVEN(FITSEVEN);
        //fcNumberModelPublic.setNumberNoMetaFITEIGHT(FITEIGHT);
        //fcNumberModelPublic.setNumberNoMetaFITNINE(FITNINE);
        //fcNumberModelPublic.setNumberNoMetaFITZEROC(FITZEROC);
        //fcNumberModelPublic.setNumberNoMetaFITONEC(FITONEC);
        //fcNumberModelPublic.setNumberNoMetaFITTWOC(FITTWOC);
        //fcNumberModelPublic.setNumberNoMetaFITTHREEC(FITTHREEC);
        //fcNumberModelPublic.setNumberNoMetaFITFOURC(FITFOURC);
        //fcNumberModelPublic.setNumberNoMetaFITFIVEC(FITFIVEC);
        //fcNumberModelPublic.setNumberNoMetaFITSIXC(FITSIXC);
        //fcNumberModelPublic.setNumberNoMetaFITSEVENC(FITSEVENC);
        //fcNumberModelPublic.setNumberNoMetaFITEIGHTC(FITEIGHTC);
        //fcNumberModelPublic.setNumberNoMetaFITNINEC(FITNINEC);
        //SOPL.accept(FCNumberInvockPublic.getInitObject(fcNumberModelPublic));
        //SOPL.accept(FCVariableInvockPublic.getInitClass(FCVariableModelPublic.class));
        //FCVariableModelPublic fcVariableModelPublic=new FCVariableModelPublic();
        //fcVariableModelPublic.setVariableSUCCESS(SUCCESS);
        //fcVariableModelPublic.setVariableFAILURE(FAILURE);
        //fcVariableModelPublic.setVariableINITVOIDSTATIC(INITVOIDSTATIC);
        //fcVariableModelPublic.setVariableINITBASICSTATIC(INITBASICSTATIC);
        //fcVariableModelPublic.setVariableINITNOPARAMSTATIC(INITNOPARAMSTATIC);
        //fcVariableModelPublic.setVariableINITNORETURNSTATIC(INITNORETURNSTATIC);
        //fcVariableModelPublic.setVariableINITVOIDDAFAULT(INITVOIDDAFAULT);
        //fcVariableModelPublic.setVariableINITBASICDAFAULT(INITBASICDAFAULT);
        //fcVariableModelPublic.setVariableINITNOPARAMDAFAULT(INITNOPARAMDAFAULT);
        //fcVariableModelPublic.setVariableINITNORETURNDAFAULT(INITNORETURNDAFAULT);
        //
        //SOPL.accept(FCVariableInvockPublic.getInitObject(fcVariableModelPublic));
        //SOPL.accept(FCWeekInvockPublic.getInitClass(FCWeekModelPublic.class));
        //FCWeekModelPublic fcWeekIModelPublic=new FCWeekModelPublic();
        //fcWeekIModelPublic.setWeekNoMetaSUNDAY(SUNDAY);
        //fcWeekIModelPublic.setWeekNoMetaMONDAY(MONDAY);
        //fcWeekIModelPublic.setWeekNoMetaTUESDAY(TUESDAY);
        //fcWeekIModelPublic.setWeekNoMetaWEDNESDAY(WEDNESDAY);
        //fcWeekIModelPublic.setWeekNoMetaTHURSDAY(THURSDAY);
        //fcWeekIModelPublic.setWeekNoMetaFRIDAY(FRIDAY);
        //fcWeekIModelPublic.setWeekNoMetaSATURDAY(SATURDAY);
        //fcWeekIModelPublic.setWeekNoMetaSUNDAYC(SUNDAYC);
        //fcWeekIModelPublic.setWeekNoMetaMONDAYC(MONDAYC);
        //fcWeekIModelPublic.setWeekNoMetaTUESDAYC(TUESDAYC);
        //fcWeekIModelPublic.setWeekNoMetaWEDNESDAYC(WEDNESDAYC);
        //fcWeekIModelPublic.setWeekNoMetaTHURSDAYC(THURSDAYC);
        //fcWeekIModelPublic.setWeekNoMetaFRIDAYC(FRIDAYC);
        //fcWeekIModelPublic.setWeekNoMetaSATURDAYC(SATURDAYC);
        //SOPL.accept(FCWeekInvockPublic.getInitObject(fcWeekIModelPublic));
    }
}
