package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.Interpreter;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.Interpreter
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 14 时 14 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@Transactional
public class FitCoreBasicContentImpl{
    private Double num1;
    private Double num2;

    /**
     * Instantiates a new Fit core basic content.
     *
     * @param num1
     *         the num 1
     * @param num2
     *         the num 2
     */
    public FitCoreBasicContentImpl(Double num1,Double num2){
        SOPL.accept("这里获得需要处理的元数据，第一个："+num1+"，第二个："+num2);
        this.num1=num1;
        this.num2=num2;
    }
}
