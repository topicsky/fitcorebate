package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.invock;

import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCExtensionNameModelPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process.FCExtensionNameProcessPublic;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.invock
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 17 时 07 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
public class FCExtensionNameInvockPublic{
    /**
     * Gets init class.
     *
     * @param fcExtensionNameModelPublicClass
     *         the fc extension name model public class
     *
     * @return the init class
     *
     * @throws Exception
     *         the exception
     */
    public static FCExtensionNameModelPublic getInitClass(Class<FCExtensionNameModelPublic> fcExtensionNameModelPublicClass) throws Exception{
        return FCExtensionNameProcessPublic.getFCExtensionNamePublicInfo(FCExtensionNameModelPublic.class);
    }

    /**
     * Gets init object.
     *
     * @param fitCoreExtensionNameImpl
     *         the fit core extension name
     *
     * @return the init object
     *
     * @throws Exception
     *         the exception
     */
    public static FCExtensionNameModelPublic getInitObject(FCExtensionNameModelPublic fitCoreExtensionNameImpl) throws Exception{
        return FCExtensionNameProcessPublic.getFCExtensionNamePublicInfoObject(fitCoreExtensionNameImpl);
    }
}
