package top.topicsky.www.fitcore.system.kernel.meta.custom.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.custom.core.FitCoreKeyMate;
import top.topicsky.www.fitcore.system.kernel.meta.custom.core.FitCoreNotRecordMate;
import top.topicsky.www.fitcore.system.kernel.meta.custom.core.FitCoreTableMate;

import java.io.Serializable;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.custom.model
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 14 时 09 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
@FitCoreTableMate(name="FitCoreUser")
public class FitCoreUserImpl implements Serializable{
    @FitCoreKeyMate
    private String id;
    private String name;
    @FitCoreNotRecordMate
    private String sex;
    private int age;
}
