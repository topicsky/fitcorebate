package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCExceptionPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCExceptionModelPublic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.OTS;
import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.STCHAR;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FCExceptionProcessPublic implements Serializable{
    /**
     * The Fc exception model public golbal.
     */
    static Supplier<FCExceptionModelPublic> fcExceptionModelPublicGolbal=()->new FCExceptionModelPublic();
    /**
     * The Clazz fc exception public.
     */
    static Class<FCExceptionPublic> clazzFCExceptionPublic=FCExceptionPublic.class;

    /**
     * Gets fc exception public info.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fc exception public info
     *
     * @throws NoSuchMethodException
     *         the no such method exception
     * @throws InvocationTargetException
     *         the invocation target exception
     * @throws IllegalAccessException
     *         the illegal access exception
     */
    public static FCExceptionModelPublic getFCExceptionPublicInfo(Class<?> clazz) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException{
        FCExceptionModelPublic fcExceptionModelPublic=fcExceptionModelPublicGolbal.get();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FCExceptionPublic annotationFCExceptionPublicField=field.getAnnotation(clazzFCExceptionPublic);
            boolean annotationFCExceptionPublicBoolean=field.isAnnotationPresent(clazzFCExceptionPublic);
            Class fcExceptionModelPublicClazz=fcExceptionModelPublic.getClass();
            Method m1=fcExceptionModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCExceptionPublicBoolean){
                m1.invoke(fcExceptionModelPublic,annotationFCExceptionPublicField.exceptionExp());
            }
        }
        return fcExceptionModelPublic;
    }

    /**
     * Gets fc exception public info object.
     *
     * @param object
     *         the object
     *
     * @return the fc exception public info object
     *
     * @throws Exception
     *         the exception
     */
    public static FCExceptionModelPublic getFCExceptionPublicInfoObject(Object object) throws Exception{
        FCExceptionModelPublic fcExceptionModelPublic=fcExceptionModelPublicGolbal.get();
        Field[] fields=object.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            Object filedstemp=field.get(object);
            FCExceptionPublic annotationFCExceptionPublicField=field.getAnnotation(clazzFCExceptionPublic);
            boolean annotationFCExceptionPublicBoolean=field.isAnnotationPresent(clazzFCExceptionPublic);
            Class fcExceptionModelPublicClazz=fcExceptionModelPublic.getClass();
            Method m1=fcExceptionModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCExceptionPublicBoolean){
                if(filedstemp!=null){
                    m1.invoke(fcExceptionModelPublic,OTS.apply(filedstemp));
                }else{
                    m1.invoke(fcExceptionModelPublic,annotationFCExceptionPublicField.exceptionExp());
                }
            }else{
                if(filedstemp!=null){
                    m1.invoke(fcExceptionModelPublic,OTS.apply(filedstemp));
                }
            }
        }
        return fcExceptionModelPublic;
    }
}
