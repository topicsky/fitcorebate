package top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.core.FCWeekPublic;
import top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.model.FCWeekModelPublic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.function.FunctionUtil.STCHAR;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.meta.original.xconstant.xpublic.process
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 07 月 07 日 15 时 32 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FCWeekProcessPublic implements Serializable{
    /**
     * The Fc week model public golbal.
     */
    static Supplier<FCWeekModelPublic> fcWeekModelPublicGolbal=()->new FCWeekModelPublic();
    /**
     * The Clazz fc week public.
     */
    static Class<FCWeekPublic> clazzFCWeekPublic=FCWeekPublic.class;

    /**
     * Gets fc week public info.
     *
     * @param clazz
     *         the clazz
     *
     * @return the fc week public info
     *
     * @throws Exception
     *         the exception
     */
    public static FCWeekModelPublic getFCWeekPublicInfo(Class<?> clazz) throws Exception{
        FCWeekModelPublic fcWeekModelPublic=fcWeekModelPublicGolbal.get();
        Field[] fields=clazz.getDeclaredFields();
        for(Field field : fields){
            FCWeekPublic annotationFCWeekPublicField=field.getAnnotation(clazzFCWeekPublic);
            boolean annotationFCWeekPublicBoolean=field.isAnnotationPresent(clazzFCWeekPublic);
            Class fcWeekModelPublicClazz=fcWeekModelPublic.getClass();
            Method m1=fcWeekModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCWeekPublicBoolean){
                m1.invoke(fcWeekModelPublic,annotationFCWeekPublicField.weekExpString());
            }
        }
        return fcWeekModelPublic;
    }

    /**
     * Gets fc week public info object.
     *
     * @param object
     *         the object
     *
     * @return the fc week public info object
     *
     * @throws Exception
     *         the exception
     */
    public static FCWeekModelPublic getFCWeekPublicInfoObject(Object object) throws Exception{
        FCWeekModelPublic fcWeekModelPublic=fcWeekModelPublicGolbal.get();
        Field[] fields=object.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            Object filedstemp=field.get(object);
            boolean annotationFCWeekPublicBoolean=field.isAnnotationPresent(clazzFCWeekPublic);
            Class fcWeekModelPublicClazz=fcWeekModelPublic.getClass();
            Method m1=fcWeekModelPublicClazz.getDeclaredMethod("set"+String.valueOf(STCHAR.apply(field.getName())),String.class);
            if(annotationFCWeekPublicBoolean){
                if(filedstemp!=null){
                    FCWeekProcessPublic.initProcessClass(m1,fcWeekModelPublic,field,filedstemp);
                }else{
                    FCWeekProcessPublic.initProcessClass(m1,fcWeekModelPublic,field,filedstemp);
                }
            }else{
                if(filedstemp!=null){
                    FCWeekProcessPublic.initProcessClass(m1,fcWeekModelPublic,field,filedstemp);
                }
            }
        }
        return fcWeekModelPublic;
    }

    /**
     * Init process class.
     *
     * @param <SIC>
     *         the type parameter
     * @param method
     *         the method
     * @param sic
     *         the sic
     * @param field
     *         the field
     * @param filedstemp
     *         the filedstemp
     *
     * @throws InvocationTargetException
     *         the invocation target exception
     * @throws IllegalAccessException
     *         the illegal access exception
     * @throws NoSuchMethodException
     *         the no such method exception
     * @throws InstantiationException
     *         the instantiation exception
     */
    public static <SIC> void initProcessClass(Method method,SIC sic,Field field,Object filedstemp) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, InstantiationException{
        if(filedstemp!=null){
            method.invoke(sic,filedstemp);
        }else{
            String simpleName=field.getType().getSimpleName();
            switch(simpleName){
                case "String":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpString());
                    break;
                case "int":
                case "Integer":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpInt());
                    break;
                case "float":
                case "Float":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpFloat());
                    break;
                case "boolean":
                case "Boolean":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpBoolean());
                    break;
                case "byte":
                case "Byte":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpByte());
                    break;
                case "double":
                case "Double":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpDouble());
                    break;
                case "char":
                case "Character":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpChar());
                    break;
                case "long":
                case "Long":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpLong());
                    break;
                case "short":
                case "Short":
                    method.invoke(sic,field.getAnnotation(clazzFCWeekPublic).weekExpShort());
                    break;
                case "String[]":
                    method.invoke(sic,(Object[])field.getAnnotation(clazzFCWeekPublic).weekExpStringList());
                    break;
                case "int[]":
                case "Integer[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpIntList());
                    break;
                case "float[]":
                case "Float[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpFloatList());
                    break;
                case "boolean[]":
                case "Boolean[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpBooleanList());
                    break;
                case "byte[]":
                case "Byte[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpByteList());
                    break;
                case "double[]":
                case "Double[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpDoubleList());
                    break;
                case "char[]":
                case "Character[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpCharList());
                    break;
                case "long[]":
                case "Long[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpLongList());
                    break;
                case "short[]":
                case "Short[]":
                    method.invoke(sic,(Object)field.getAnnotation(clazzFCWeekPublic).weekExpShortList());
                    break;
            }
        }
    }
}
