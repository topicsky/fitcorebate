package top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.mediator;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.function.Supplier;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.behavioral.middleware.mediator
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 30 日 11 时 48 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@Component
@Transactional
public class FitCoreBasicMediatorBeanImpl implements FitCoreBasicMediatorInter, Serializable{
    /**
     * The Supplier inherit xa.
     */
    Supplier<FitCoreBasicObjectAbstractImpl> supplierInheritXA=()->new FitCoreBasicObjectInheritXAImpl(this);
    /**
     * The Fit core basic object inherit xa bean.
     */
    FitCoreBasicObjectAbstractImpl fitCoreBasicObjectInheritXABean=supplierInheritXA.get();
    /**
     * The Supplier inherit xb.
     */
    Supplier<FitCoreBasicObjectAbstractImpl> supplierInheritXB=()->new FitCoreBasicObjectInheritXBImpl(this);
    /**
     * The Fit core basic object inherit xb bean.
     */
    FitCoreBasicObjectAbstractImpl fitCoreBasicObjectInheritXBBean=supplierInheritXB.get();
    private FitCoreBasicObjectAbstractImpl fitCoreBasicObjectInheritXA;
    private FitCoreBasicObjectAbstractImpl fitCoreBasicObjectInheritXB;

    @Override
    public void createMediator(){
        SOPL.accept("这里是中介类，构造——XA——访问对象的时候，传入中介类，统一控制");
        fitCoreBasicObjectInheritXA=fitCoreBasicObjectInheritXABean;
        SOPL.accept("这里是中介类，构造——XB——访问对象的时候，传入中介类，统一控制");
        fitCoreBasicObjectInheritXB=fitCoreBasicObjectInheritXBBean;
    }

    @Override
    public void allTakeEffect(){
        SOPL.accept("这里是中介类，调用——XA——访问对象方法");
        fitCoreBasicObjectInheritXA.takeEffect();
        SOPL.accept("这里是中介类，调用——XB——访问对象方法");
        fitCoreBasicObjectInheritXB.takeEffect();
    }
}
