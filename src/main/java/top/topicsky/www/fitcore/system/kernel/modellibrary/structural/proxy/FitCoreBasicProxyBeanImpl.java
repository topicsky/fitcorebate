package top.topicsky.www.fitcore.system.kernel.modellibrary.structural.proxy;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static top.topicsky.www.fitcore.system.kernel.common.consumer.ConsumerUtil.SOPL;

/**
 * 所在项目名称 ： fitcore
 * 操作文件所在包路径　： top.topicsky.www.fitcore.system.kernel.modellibrary.structural.proxy
 * 该类由 pxp20 创建
 * 构造时间 ：　2017 年 06 月 28 日 10 时 36 分
 * <p>
 * 作者 ：     潘小平
 * 邮箱 ：     pxp20082008@126.com
 * 组织 ：     深藏彼岸社区
 * <p>
 * 参数｛param1｝＝｛value1｝
 * 参数｛param2｝＝｛value2｝
 * 参数｛param3｝＝｛value3｝
 * 参数｛param4｝＝｛value4｝
 * 参数｛param5｝＝｛value5｝
 * 参数｛param6｝＝｛value6｝
 * ......
 */
@Component
@Transactional
public class FitCoreBasicProxyBeanImpl implements FitCoreBasicProxyInter, Serializable{
    private FitCoreBasicProxyImpl fitCoreBasicProxyImpl;

    /**
     * Instantiates a new Fit core basic proxy bean.
     *
     * @param fitCoreBasicProxyImpl
     *         the fit core basic proxy
     */
    public FitCoreBasicProxyBeanImpl(FitCoreBasicProxyImpl fitCoreBasicProxyImpl){
        super();
        this.fitCoreBasicProxyImpl=fitCoreBasicProxyImpl;
    }

    @Override
    public void fitCoreBasicProxy(){
        this.before();
        fitCoreBasicProxyImpl.fitCoreBasicProxy();
        this.atfer();
    }

    private void before(){
        SOPL.accept("这是代理类调用适配接口方法之前能操作的域");
    }

    private void atfer(){
        SOPL.accept("这是代理类调用适配接口方法之后能操作的域");
    }
}
